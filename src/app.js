function Character(charName) {
    this.statInfo
    this.charName = charName

    // Skipping incorrect information pulled about Cersei
    this.skipAliases = false

    var that = this

    this.displayBio = function() {
        that.bioEl.className = "bio activeBio"

        var infoEl = document.querySelector(".activeBio .info")

        if (that.statInfo && that.statInfo.succeeded) {
            infoEl.innerHTML = buildStatElements(that.statInfo)
        } else {

            infoEl.innerHTML = "Loading data..."

            var request = new XMLHttpRequest()
            var formattedName = that.charName.split(" ").join("+")
            request.open("GET", "https://www.anapioficeandfire.com/api/characters?name="+formattedName, true)
            request.onload = function() {

                var succeeded = that.parseResponse(request.responseText)

                if (succeeded) {
                    infoEl.innerHTML = buildStatElements(that.statInfo)
                } else {
                    infoEl.innerHTML = ""
                }
            }
            request.send()
        }
    }

    this.parseResponse = function(response) {
        that.statInfo = {}
        that.statInfo.skipAliases = that.skipAliases

        var rawJson = JSON.parse(response)[0]

        if (!rawJson) {
            that.statInfo.succeeded = false
            return false
        }

        that.statInfo.name = rawJson.name
        that.statInfo.dob = rawJson.born
        that.statInfo.aliases = rawJson.aliases.slice()
        that.statInfo.titles = rawJson.titles.slice()
        that.statInfo.culture = rawJson.culture
        that.statInfo.playedBy = rawJson.playedBy[0]
        that.statInfo.succeeded = true
        return true
    }
}

var gotManager = {}

function buildStatElements(statInfo) {
    var statEls = "<p><span class='bioTitle'>Name:</span>" + statInfo.name + "</p>"
    statEls += "<p><span class='bioTitle'>Born:</span>" + statInfo.dob + "</p>"
    statEls += "<p class='biolist bioTitle'>Aliases:</p><ul>"

    if (!statInfo.skipAliases) {
        for (var i = 0; i < statInfo.aliases.length; i++) {
            statEls += "<li>" + statInfo.aliases[i] + "</li>"
        }
    }
    statEls += "</ul><p class='biolist bioTitle'>Titles:</p><ul>"

    for (i=0; i < statInfo.titles.length; i++) {
        statEls += "<li>" + statInfo.titles[i] + "</li>"
    }

    statEls += "</ul><p><span class='bioTitle'>Culture:</span>" + statInfo.culture + "</p>"

    statEls += "<p><span class='bioTitle'>Played By:</span>" + statInfo.playedBy + "</p>"

    return statEls
}

function toggleTab(activeTab) {
    var tabs = document.querySelectorAll("nav li")
    for (var i=0; i < tabs.length; i++) {
        tabs[i].className = ""
    }

    activeTab.target.className = "activeTab"

    var bios = document.getElementsByClassName("bio")

    for (i=0; i < bios.length; i++){
        bios[i].className = "bio inactiveBio"
    }

    switch(activeTab.target.id) {
        case "jonTab":
            gotManager.jon.displayBio()
            break

         case "cerseiTab":
            gotManager.cersei.displayBio()
            break

        case "tyrionTab":
            gotManager.tyrion.displayBio()
            break

        case "aryaTab":
            gotManager.arya.displayBio()
            break

        case "joffreyTab":
            gotManager.joffrey.displayBio()
            break

        case "ramsayTab":
            gotManager.ramsay.displayBio()
            break

        case "petyrTab":
            gotManager.petyr.displayBio()
            break

        case "brienneTab":
            gotManager.brienne.displayBio()
            break

        case "sandorTab":
            gotManager.sandor.displayBio()
            break

        case "customTab":
            gotManager.custom.displayBio()
            break
    }
}

function searchCustomName() {
    if (gotManager.custom.statInfo && gotManager.custom.statInfo.succeeded) {
        if (gotManager.custom.statInfo.name.toLowerCase() === gotManager.custom.customEl.value.toLowerCase()) {
            // already displaying character, don't request new data
            return
        }
    }
    gotManager.custom.statInfo = null
    gotManager.custom.charName = gotManager.custom.customEl.value
    gotManager.custom.displayBio()
}

function ready() {
    gotManager.jon = new Character("Jon Snow")
    gotManager.jon.bioEl = document.getElementById("jonSnowBio")

    gotManager.cersei = new Character("Cersei Lannister")
    gotManager.cersei.bioEl = document.getElementById("cerseiBio")
    gotManager.cersei.skipAliases = true

    gotManager.tyrion = new Character("Tyrion Lannister")
    gotManager.tyrion.bioEl = document.getElementById("tyrionBio")

    gotManager.arya = new Character("Arya Stark")
    gotManager.arya.bioEl = document.getElementById("aryaBio")

    gotManager.joffrey = new Character("Joffrey Baratheon")
    gotManager.joffrey.bioEl = document.getElementById("joffreyBio")

    gotManager.ramsay = new Character("Ramsay Snow")
    gotManager.ramsay.bioEl = document.getElementById("ramsayBio")

    gotManager.petyr = new Character("Petyr Baelish")
    gotManager.petyr.bioEl = document.getElementById("petyrBio")

    gotManager.brienne = new Character("Brienne of Tarth")
    gotManager.brienne.bioEl = document.getElementById("brienneBio")

    gotManager.sandor = new Character("Sandor Clegane")
    gotManager.sandor.bioEl = document.getElementById("sandorBio")

    gotManager.custom = new Character("custom")
    gotManager.custom.bioEl = document.getElementById("customBio")
    gotManager.custom.customEl = document.querySelector("#customSearchContainer input")

    document.querySelector("#customSearchContainer button").addEventListener("click", searchCustomName)
    document.querySelector("#customSearchContainer input").addEventListener("keypress", function(e) {
        if (e.key === "Enter") {
            searchCustomName()
        }
    })

    var tabs = document.querySelectorAll("nav li")

    for (var i=0; i < tabs.length; i++) {
        tabs[i].addEventListener("click", toggleTab)
    }

    gotManager.jon.displayBio()
}

if(document.readyState !== "loading") {
    ready()
} else {
    document.addEventListener("DOMContentLoaded", ready)
}